package shoppingcart;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class Controller extends JFrame {
	
	private ShoppingCart cartModel;
	private View view;
	
	public Controller(String string) {
		super(string); //Give window title
		
		this.cartModel = new ShoppingCart();
		this.view = new View(this.cartModel);
		
		//Setup Window
		getContentPane().add(this.view);
		setSize(450,300);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
			
		view.getLeftButton().addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				eventHandlerLeftButton();				
			}
		});
		
		view.getRightButton().addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				eventHandlerRightButton();				
			}
		});
		
	}
	
	public void eventHandlerLeftButton() {
		String item = view.getShoppingList().getSelectedValue().toString();
		
		cartModel.removeFromCart(item);
		view.update();
	}
	
	public void eventHandlerRightButton() {
		String item = view.getAvaliableItems().getSelectedValue().toString();
		
		cartModel.addToCart(item);
		view.update();
	}

	public static void main(String[] args) {
		JFrame app = new Controller("Shopping Cart");
		app.setVisible(true);
	}
	

}
