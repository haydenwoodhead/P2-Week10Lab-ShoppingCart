package shoppingcart;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;

public class View extends JPanel {
	
	private ShoppingCart cartModel;
	private JList shoppingList;
	private JList avaliableItems;
	private JButton leftButton;
	private JButton rightButton;
	
	public View(ShoppingCart cartModel) {
		super();
		
		this.cartModel = cartModel;
		
		this.setLayout(null);
		
		this.shoppingList = new JList();
		shoppingList.setLocation(285,30);
		shoppingList.setSize(150, 200);
		add(shoppingList);
		
		this.avaliableItems = new JList();
		avaliableItems.setLocation(15,30);
		avaliableItems.setSize(150, 200);
		add(avaliableItems);
		
		this.leftButton = new JButton("<-");
		leftButton.setLocation(200, 75);
		leftButton.setSize(50, 30);
		add(leftButton);
		
		this.rightButton = new JButton("->");
		rightButton.setLocation(200, 125);
		rightButton.setSize(50, 30);
		add(rightButton);
		
		
		this.update();
		
	}
	
	public void update() {
		String[] shoppingCartItems = cartModel.getShoppingCart();
		String[] avaliableItems = cartModel.getAvaliableItems();

		if (shoppingCartItems.length < 1) {
			this.leftButton.setEnabled(false);
		}
		else {
			this.leftButton.setEnabled(true);
		}
		
		if (avaliableItems.length < 1) {
			this.rightButton.setEnabled(false);
		}
		else {
			this.rightButton.setEnabled(true);
		}
		
		this.shoppingList.setListData(shoppingCartItems);
		this.avaliableItems.setListData(avaliableItems);
	}

	/*
	 * Return the move left button
	 */
	public JButton getLeftButton() {
		return this.leftButton;
	}

	/*
	 * return the move right button
	 */
	public JButton getRightButton() {
		return this.rightButton;
	}

	public JList getShoppingList() {
		return this.shoppingList;
	}

	public JList getAvaliableItems() {
		return this.avaliableItems;
	}

}
