// vinaidu@aut.ac.nz

package shoppingcart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ShoppingCart {
		
	private ArrayList<String> shoppingCart;
	private ArrayList<String> avaliableItems;
	
	public ShoppingCart() {
		this.avaliableItems = new ArrayList<String>();
		this.shoppingCart = new ArrayList<String>();
		
		this.avaliableItems.add("Books");
		this.avaliableItems.add("Movies");
		/*this.avaliableItems.add("Bikes");
		this.avaliableItems.add("Food");
		this.avaliableItems.add("Scooter");
		this.avaliableItems.add("Cake");
		this.avaliableItems.add("Plane");
		this.avaliableItems.add("Helicopter");*/

	}
	
	/*
	 * Probably a better way to do this
	 */
	public void addToCart(String item) {
		boolean notUpdated = true;
		
		for (int i = 0; i < this.avaliableItems.size() && notUpdated; i++) {
			if (this.avaliableItems.get(i).equals(item)) {
				this.avaliableItems.remove(i);
				this.shoppingCart.add(item);
				
				this.avaliableItems.removeAll(Arrays.asList("", null));
				
				notUpdated = false;
			}
		}
	}
	
	public void removeFromCart(String item) {
		boolean notUpdated = true;
		
		for (int i = 0; i < this.shoppingCart.size() && notUpdated; i++) {
			if (this.shoppingCart.get(i).equals(item)) {
				this.shoppingCart.remove(i);
				this.avaliableItems.add(item);
				
				notUpdated = false;
			}
		}
	}

	public String[] getShoppingCart() {
		return this.shoppingCart.toArray(new String[this.shoppingCart.size()]);
	}

	public String[] getAvaliableItems() {
		return this.avaliableItems.toArray(new String[this.avaliableItems.size()]);
	}

}
